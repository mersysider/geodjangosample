# from django.contrib import admin

# Register your models here.
from .models import Provider, Search

from django.contrib.gis import admin

class ProviderAdmin(admin.OSMGeoAdmin):
	list_display = ("name","email","phone")

admin.site.register(Provider,ProviderAdmin)
