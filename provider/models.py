from __future__ import unicode_literals
from django.contrib.gis.db import models

# Create your models here.

class Provider(models.Model):
	# model of the provider

	name = models.CharField(max_length=200)
	email = models.EmailField()
	phone = models.CharField(max_length=25)
	language = models.CharField(max_length=400)
	currency = models.CharField(max_length=500)
	mpoly = models.MultiPolygonField()

	def __unicode__(self):
		return self.name


class Search(models.Model):
	point = models.PointField()
	string="search"
	def __unicode__(self):
		return self.string

