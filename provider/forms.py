from django.contrib.gis import forms

from .models import Provider, Search


class ProviderForm(forms.ModelForm):
	class Meta:
		model = Provider
		fields = ('name','email','phone','language','currency','mpoly')


class SearchForm(forms.ModelForm):
	class Meta:
		model = Search
		fields = ('point',)
