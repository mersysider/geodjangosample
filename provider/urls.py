from django.conf.urls import url
from .views import (all_providers, provider_details, 
	create_provider, search_provider, update_provider, delete_provider)

urlpatterns = [

    url(r'^all/', all_providers , name='all-providers'),   
    url(r'^get/(?P<provider_id>\d+)/$',provider_details,name='provider-details'),
    url(r'^search/', search_provider , name='search-providers'), 
    url(r'^create/', create_provider , name='create-providers'), 
    url(r'^update/(?P<provider_id>\d+)/$', update_provider , name='update-provider'),
    url(r'^delete/(?P<provider_id>\d+)/$', delete_provider , name='delete-provider'),
    ]

