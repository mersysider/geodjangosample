from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponseRedirect
from .forms import ProviderForm, SearchForm
from .models import Provider
from django.template.context_processors import csrf
from django.contrib.gis.db.models.functions import *
from django.views.generic import View , TemplateView
from django.template import RequestContext

# Create your views here.

def all_providers(request):
	# pagination required
	'''this function shows provider page'''

	return render_to_response('providers.html',{'providers':Provider.objects.annotate(json=AsGeoJSON('mpoly'))})

def provider_details(request, provider_id=1):
	'''sends the requested (GET) provider to a different page'''
	return render_to_response('provider.html',
		{'provider': Provider.objects.get(id=provider_id)})


'''from modelForm=ProviderForm'''
def create_provider(request):
	if request.POST:
		form = ProviderForm(request.POST)
		if form.is_valid():
			form.save()
			# send to all provider list page
			return HttpResponseRedirect('/provider/all')
	else:
		form = ProviderForm()

	args = {}
	args.update(csrf(request))
	args['form'] = form

	return render_to_response('create_provider.html',args)

def search_provider(request):
	if request.POST:
		form = SearchForm(request.POST)
		if form.is_valid():
			pnt=form.save(commit=False)
			print pnt.point
			sm=Provider.objects.get(id=1)
			print Provider.objects.filter(mpoly__contains=pnt.point)
			# send to the list of results/outcomes page!!!!!!!!!!!!!!!!!!!!!!!!!!
			return render_to_response('geoformtest.html',
				{'providers':Provider.objects.filter(mpoly__contains=pnt.point),'pntPoint':pnt.point})
	else:
		form = SearchForm()

	args = {}
	args.update(csrf(request))
	args['form'] = form

	return render_to_response('search_provider.html',args)		


def update_provider(request, provider_id=1):
	# a is the provider object
	if provider_id:
		a = Provider.objects.get(id=provider_id)
		if request.method=="POST":
			form = ProviderForm(request.POST)

			if form.is_valid():
				b=form.save(commit=False)
				# save the instance of the new form without committing and use it to update the Provider obj in databas
				name2 = b.name
				a.name=name2
				email2 = b.email
				a.email = email2
				phone2 = b.phone
				a.phone = phone2
				language2 = b.language
				a.language = language2
				currency2 = b.currency
				a.currency = currency2
				mpoly2= b.mpoly
				a.mpoly = mpoly2
				a.save()
				return HttpResponseRedirect('/provider/get/%s' %provider_id)
		else:		
			form = ProviderForm()			
	else:
			form = ProviderForm()	

	args = {}
	args.update(csrf(request))
	args['form'] = form
	args['provider'] = Provider.objects.get(id=provider_id)

	return render_to_response('update_provider.html',args)

def delete_provider(request, provider_id=1):

	if provider_id:
		Provider.objects.get(id=provider_id).delete()
		return HttpResponseRedirect('/provider/all')
	else:
		return render_to_response('all-providers',{})