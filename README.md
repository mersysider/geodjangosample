# README #

### What is this repository for? ###

* This repository includes a geodjango sample page, which lets you create a provider with a particular service area demarcated on a map, update and delete that provider. This app also lets users search for providers by clicking on a point on a map, after the submission, the user is redirected to the results page with the providers who provide their service in that particular area.
* [Manish Manandhar](https://mersysider@bitbucket.org/mersysider/geodjangosample.git)

### How do I get set up? ###
(In Ubuntu 14.04)
* $ virtualenv venv

* $ source venv/bin/activate

* $ pip install -r requirements.txt

* Install spatialite database 
(https://docs.djangoproject.com/en/1.9/ref/contrib/gis/install/spatialite/#spatialite-source)

* $ python manage.py makemigrations

* $ python manage.py sqlmigrate provider 0001

* $ python manage.py migrate

* $ python manage.py runserver


(in your web browser)

*localhost:8000/provider/all

or admin page,

*localhost:8000/admin/